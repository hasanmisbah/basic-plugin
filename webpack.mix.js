/* eslint-disable */
let mix = require('laravel-mix');
let path = require('path');
const webpackConfig = require('./webpack.config.js');

mix.js('resources/script/app.js', 'js').vue({ version: 3 })
  .sass('resources/stylesheet/app.scss', 'css')
  .extract()
  .setPublicPath('public')
  .webpackConfig(webpackConfig)
;
