<?php

defined('ABSPATH') or die('Direct access not allowed here!');

return function ($path) {

    // Create a new instance of Application class
    $app = new HasanMisbah\Core\Foundation\Application($path);

    // create a new instance of WPKernel class in app/Core/Kernels/WPKernel.php
    $kernel = $app->make(HasanMisbah\Core\Kernels\WPKernel::class);
    // Lets kernel handle the request
    $kernel->handle();
};
