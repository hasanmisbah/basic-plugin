/* eslint-disable */
const path = require('path');

module.exports = {

  stats: {
    warnings: false,
  },

  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'resources/admin'),
      '~': path.resolve(__dirname, 'src/node_modules')
    },
  },
};
