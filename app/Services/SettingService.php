<?php

namespace HasanMisbah\Services;

use HasanMisbah\Core\Foundation\Application;

class SettingService
{
    /**
     * @var Application
     */
    private $app = null;

    /**
     * @var string
     */
    private $settingsKey = null;

    private $fields = [
        'emails',
        'show_date_in_format',
        'show_row_in_table',
    ];

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->settingsKey = $this->app->config()->slug . '_settings';
    }

    /**
     * @return array
     */
    public function get()
    {

        $settings = $this->app->getOption(
            $this->settingsKey,
            $this->getDefaultSetting()
        );

        return maybe_unserialize($settings);
    }

    /**
     * @param array $settings
     * @return array
     */
    public function update($settings)
    {
        $settingsToUpdate = $this->validateSetting($settings);
        $settings = wp_parse_args($settings, $settingsToUpdate);

        $this->app->updateOption($this->settingsKey, maybe_serialize($settings));

        return $settings;
    }

    /**
     * @return array
     */
    private function getDefaultSetting()
    {
        $settings = [
            'emails'              => [],
            'show_date_in_format' => 'human_readable',
            'show_row_in_table'   => 5
        ];

        return $settings;
    }

    /**
     * @param array $userSetting
     * @return array
     */
    private function validateSetting($userSetting)
    {
        $setting = [];

        foreach ($this->fields as $field) {

            if (isset($userSetting[$field])) {
                $setting[$field] = $userSetting[$field];
            }

        }

        return $setting;
    }
}
