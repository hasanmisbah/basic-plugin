<?php

namespace HasanMisbah\Services;

use HasanMisbah\Core\Support\Cache;
use HasanMisbah\Core\Support\Http;

class ApiDataService
{
    public function get()
    {
        $data = Cache::remember('test', function () {
            $response = Http::get('https://miusage.com/v1/challenge/2/static/');
            return $response['body'];
        });

        return $data;
    }

    public function getGraph()
    {
        $data = $this->get();
        return $data['graph'];
    }

    public function getPages()
    {
        $data = $this->get();
        return $data['table'];
    }
}
