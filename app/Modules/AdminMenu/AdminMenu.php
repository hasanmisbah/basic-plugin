<?php

namespace HasanMisbah\Modules\AdminMenu;

use HasanMisbah\Core\Foundation\Application;

class AdminMenu
{
    /**
     * @var Application $app
     * */
    public $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->app->addAction('admin_menu', [$this, 'register']);
        $this->app->addAction('admin_enqueue_scripts', [$this, 'loadAssets']);
    }

    public function register()
    {

        return $this->app->addMenuPage(
            'Hasan Misbah',
            'Hasan Misbah',
            'manage_options',
            'hasanmisbah',
            [$this, 'renderPage'],
            $this->getIcon()
        );
    }

    public function getIcon()
    {
        return 'dashicons-rest-api';
    }

    public function loadAssets()
    {
        $publicUrl = $this->app->path()->baseUrl . '/public';
        $pluginVersion = $this->app->config()->version;
        $pluginSlug = $this->app->config()->slug;

        $restNamespace = $this->app->config()->rest['namespace'];
        $restVersion = $this->app->config()->rest['version'];

        $this->app->enqueueScript("{$pluginSlug}-vendor", $publicUrl . '/js/vendor.js', ['jquery'], $pluginVersion, true);
        $this->app->enqueueScript("{$pluginSlug}-manifest", $publicUrl . '/js/manifest.js', ['jquery', "{$pluginSlug}-vendor"], $pluginVersion, true);
        $this->app->enqueueScript("{$pluginSlug}-app", $publicUrl . '/js/app.js', ['jquery', "{$pluginSlug}-manifest", "{$pluginSlug}-vendor"], $pluginVersion, true);

        $this->app->enqueueStyle("{$pluginSlug}-app", $publicUrl . '/css/app.css', [], $pluginVersion);

        $this->app->localizeScript("{$pluginSlug}-app", $pluginSlug, [
            'ajax_url' => admin_url('admin-ajax.php'),
            'nonce'    => wp_create_nonce('wp_rest'),
            'rest'     => [
                'namespace' => $this->app->config()->rest['namespace'],
                'version'   => $this->app->config()->rest['version'],
                'url'       => rest_url($restNamespace . '/' . $restVersion),
                'base_url'  => esc_url_raw(rest_url()),
            ],
        ]);
    }

    public function renderPage()
    {
        return $this->app->view()->render('application');
    }
}
