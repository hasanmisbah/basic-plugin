<?php

namespace HasanMisbah\Abstraction;

use HasanMisbah\Core\Exceptions\HttpClientException;
use HasanMisbah\Core\Support\HttpClientDriver;

class WPHttpClient implements HttpClientDriver
{

    /**
     * @var string|null
     */
    public $baseUri = null;

    /**
     * @param string $baseUri
     */
    public function __construct($baseUri = null)
    {
        $this->baseUri = $baseUri;
    }

    /**
     * @param string $url
     * @param mixed $data
     * @return array
     * @throws \Exception
     */
    public function get($url, $data = [])
    {
        return $this->send('GET', $url);
    }

    /**
     * @param string $url
     * @param mixed $data
     * @return array
     * @throws \Exception
     */
    public function post($url, $data = [])
    {
        return $this->send('POST', $url, $data);
    }

    /**
     * @param string $url
     * @param mixed $data
     * @throws \Exception
     */
    public function put($url, $data = [])
    {
        return $this->send('PUT', $url, $data);
    }

    /**
     * @param string $url
     * @param mixed $data
     * @throws \Exception
     */
    public function patch($url, $data = [])
    {
        return $this->send('PATCH', $url, $data);
    }

    /**
     * @param string $url
     * @param mixed $data
     * @throws \Exception
     */
    public function delete($url, $data = [])
    {
        return $this->send('DELETE', $url, $data);
    }

    /**
     * @param string $method
     * @param string $url
     * @param mixed $data
     * @throws \Exception
     */
    public function send($method, $url, $data = [])
    {

        if(!in_array($method, ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])) {
            throw new \Exception('Invalid HTTP Method');
        }

        $url = $method == 'GET'? $this->buildUri($url, $data) : $this->buildUri($url);

        $response = wp_remote_request($url, [
            'method' => $method,
            'body' => $data
        ]);

        return $this->parseResponse($response);
    }

    /**
     * @param string $baseUri
     * @return void
     */
    public function setBaseUri($baseUri)
    {
        $this->baseUri = $baseUri;
    }

    /**
     * @param mixed $response
     * @return array
     * @throws \Exception
     */
    private function parseResponse($response)
    {
        if(is_wp_error($response)) {
            throw new HttpClientException($response->get_error_message(), (int) $response->get_error_code());
        }

        $body = wp_remote_retrieve_body($response);
        $code = wp_remote_retrieve_response_code($response);
        $message = wp_remote_retrieve_response_message($response);
        $headers = wp_remote_retrieve_headers($response);


        if(is_string($body) && is_array(json_decode($body, true)) && (json_last_error() == JSON_ERROR_NONE)) {
            $body = json_decode($body, true);
        }

        return [
            'body' => $body,
            'code' => $code,
            'message' => $message,
            'headers' => $headers
        ];
    }

    /**
     * @param string $uri
     * @param mixed $data
     * @return string
     */
    private function buildUri($uri, $data = [])
    {
        $uri = $this->baseUri? $this->baseUri . '/' . $uri : $uri;

        if(!empty($data)) {
            $uri .= '?' . http_build_query($data);
        }

        return esc_url_raw($uri);
    }
}
