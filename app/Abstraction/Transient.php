<?php

namespace HasanMisbah\Abstraction;

use HasanMisbah\Core\Support\CacheDriver;
use HasanMisbah\Core\Foundation\Application;

class Transient implements CacheDriver
{
    /**
     * @var string
     */
    private $key = null;
    /**
     * @var Application
     */
    private $app = null;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->key = $this->setBaseKey();
    }


    /**
     * @param string $key
     * @param mixed $data
     * @param int $expiration
     * @return $this
     */
    public function put($key, $data = [], $expiration = 0)
    {
        $this->key = $this->generateKey($key);
        set_transient($this->key, $data, $expiration);
        return $this;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return get_transient($this->generateKey($key));
    }

    /**
     * @param string $key
     * @return $this
     */
    public function delete($key)
    {
        delete_transient($this->generateKey($key));

        return $this;
    }

    /**
     * @param string $key
     * @return string
     */
    private function generateKey($key)
    {
        $namespace = $this->app->config()->slug;
        return "{$namespace}_{$key}";
    }

    /**
     * @return string
     */
    private function setBaseKey()
    {
        $namespace = $this->app->config()->slug;
        $this->key = "{$namespace}";
        return $this->key;
    }

    /**
     * @param string $key
     * @param callable $callback
     * @param int $expiration
     * @return mixed
     */
    public function remember($key, $callback, $expiration = HOUR_IN_SECONDS)
    {
        $data = $this->get($key);

        if(!$data) {
            $data = $callback();
            $this->put($key, $data, $expiration);
        }

        return $data;
    }
}
