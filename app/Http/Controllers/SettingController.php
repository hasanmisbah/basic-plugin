<?php

namespace HasanMisbah\Http\Controllers;

use HasanMisbah\Core\Request\Request;
use HasanMisbah\Exceptions\ValidationException;
use HasanMisbah\Services\SettingService;

class SettingController extends Controller
{
    public function index(SettingService $setting)
    {
        return [
          'data' => $setting->get()
        ];
    }

    /**
     * @param Request $request
     * @param SettingService $setting
     * @return array
     * @throws ValidationException
     */
    public function update(Request $request, SettingService $setting)
    {
        $data = $request->getSanitized([
            'emails'              => 'sanitize_email',
            'show_date_in_format' => 'sanitize_text_field',
            'show_row_in_table'   => 'intval'
        ]);

        $dataToUse = $this->validate($data);
        $setting->update($dataToUse);

        return [
            'data' => $setting->get()
        ];

    }


    /**
     * @throws ValidationException
     */
    private function validate($data)
    {
        $errors = [];

        if(!isset($data['emails'])) {
            $errors[] = 'Emails is required';
        }

        if(!is_array($data['emails'])) {
            $errors[] = 'Emails must be an array';
        }

        if(!isset($data['show_date_in_format'])) {
            $errors[] = 'Show date in format is required';
        }

        if(!is_string($data['show_date_in_format'])) {
            $errors[] = 'Show date in format must be a string';
        }

        if(!isset($data['show_row_in_table'])) {
            $errors[] = 'Show row in table is required';
        }

        if(!is_int($data['show_row_in_table'])) {
            $errors[] = 'Show row in table must be an integer';
        }

        if(isset($data['emails'])) {
            $errors = array_merge($errors, $this->validateEmail($data['emails']));
        }

        if(isset($data['show_date_in_format'])) {
            $errors = array_merge($errors, $this->validateShowDateInFormat($data['show_date_in_format']));
        }

        if(isset($data['show_row_in_table'])) {
            $errors = array_merge($errors, $this->validateShowRowInTable($data['show_row_in_table']));
        }

        // if errors then throw exception with all errors else return true
        if (count($errors) > 0) {
            throw new ValidationException('Invalid entity', $errors);
        }

        return $data;
    }

    private function validateEmail($emails)
    {
        $errors = [];

        if (count($emails) < 1 || count($emails) > 5) {
            $errors[] = 'Emails must be between 1 and 5';
            return $errors;
        }

        foreach ($emails as $email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errors[] = $email . ' is not a valid email';
            }
        }

        return $errors;
    }

    private function validateShowDateInFormat($showDateInFormat)
    {
        $errors = [];

        if (!in_array($showDateInFormat, ['human_readable', 'timestamp'])) {
            $errors[] = $showDateInFormat . ' is not a valid format';
        }

        return $errors;
    }

    private function validateShowRowInTable($showRowInTable)
    {
        $errors = [];

        if (!is_int($showRowInTable)) {
            $errors[] = $showRowInTable . ' is not a valid number';
        }

        return $errors;
    }
}
