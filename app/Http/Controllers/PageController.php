<?php

namespace HasanMisbah\Http\Controllers;

use HasanMisbah\Services\ApiDataService;

class PageController extends Controller
{
    public function index(ApiDataService $apiDataService)
    {
        $data = $apiDataService->getPages();

        return [
            'data' => $data
        ];
    }
}
