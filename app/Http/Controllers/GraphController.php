<?php

namespace HasanMisbah\Http\Controllers;

use HasanMisbah\Services\ApiDataService;

class GraphController extends Controller
{
    public function index(ApiDataService $apiDataService)
    {
        $data = $apiDataService->getGraph();

        return [
            'data' => $data
        ];
    }
}
