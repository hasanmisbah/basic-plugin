<?php

namespace HasanMisbah\Core\View;

use HasanMisbah\Core\Foundation\Application;

class View
{
    /**
     * @var \HasanMisbah\Core\Foundation\Application $app
     * */
    protected $app;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param $view
     * @param $data
     * @return $this
     * @throws \Exception
     */
    public function render($view, $data = [])
    {
        $content = $this->get($view, $data);
        echo $content;
        return $this;
    }

    /**
     * @param $view
     * @param $data
     * @return false|string
     * @throws \Exception
     */
    public function get($view, $data = [])
    {
        $view = str_replace('.', '/', $view);
        $view = $this->app->path->view . '/' . $view . '.php';

        if (!file_exists($view)) {
            throw new \Exception("View {$view} not found");
        }

        extract($data);

        ob_start();
        include $view;
        $content = ob_get_clean();

        return $content;
    }
}
