<?php

namespace HasanMisbah\Core\Foundation;

use HasanMisbah\Core\Container\Container;
use HasanMisbah\Core\Request\Request;
use HasanMisbah\Core\Response\Response;
use HasanMisbah\Core\Routing\Router;
use HasanMisbah\Core\Support\AccessContainer;
use HasanMisbah\Core\Support\Cache;
use HasanMisbah\Core\Support\Http;
use HasanMisbah\Core\View\View;
use HasanMisbah\Hooks\ActivationHandler;
use HasanMisbah\Hooks\DeactivationHandler;
use HasanMisbah\ServiceProvider;

/**
 * @method static make($abstract, $parameters = [])
 * @method static get($abstract)
 */
class Application
{
    use HookBinder, ForwardCall;

    /**
     * @var Container $container
     * */
    private $container;
    /**
     * @var AccessContainer $config
     */
    public $config;

    /**
     * @var AccessContainer $config
     */
    public $path;

    /**
     * @var View $view
     */
    public $view;


    /**
     * @param $path
     * @throws \ReflectionException
     */
    public function __construct($path = null)
    {
        $this->container = new Container;
        $this->config = new AccessContainer();
        $this->path = new AccessContainer();
        $this->view = new View($this);
        $this->init($path);
    }

    /**
     * @param $path
     * @return void
     * @throws \ReflectionException
     */
    protected function init($path)
    {
        $this->bindPath($path);
        $this->loadConfig();
        $this->bindComponents();
        $this->registerRoutes();
        $this->loadHooks();

    }

    /**
     * @return void
     * @throws \ReflectionException
     */
    protected function bindComponents()
    {
        $this->container->singleton(Application::class, $this);
        $this->container->bind(__CLASS__, $this);
        $this->container->singleton(Request::class, Request::class);
        $this->container->singleton(Response::class, Response::class);
        $this->container->singleton(View::class, View::class);

        $this->container->setAlias('view', View::class);
        $this->container->setAlias('app', $this);
        $this->container->setAlias('request', Request::class);
        $this->container->setAlias('response', Response::class);


        if ($this->config()->drivers['cache'] && class_exists($this->config()->drivers['cache'])) {

            $this->container->bind(Cache::class, function () {
                $driver = $this->config()->drivers['cache'];
                $this->container->bind($driver);
                $driverObj = $this->container->make($driver);
                return new Cache($this, $driverObj);
            });

            $this->container->setAlias('cache', Cache::class);
        }


        if ($this->config()->drivers['http'] && class_exists($this->config()->drivers['http'])) {

            $this->container->bind(Http::class, function () {
                $driver = $this->config()->drivers['http'];
                $this->container->bind($driver);
                $driverObj = $this->container->make($driver);
                return new Http($this, $driverObj);
            });

            $this->container->setAlias('http', Http::class);
        }
    }

    /**
     * @param $path
     * @return void
     */
    protected function bindPath($path)
    {
        $this->path->basePath = plugin_dir_path($path);
        $this->path->baseUrl = plugin_dir_url($path);
        $this->path->file = $path;
        $this->path->asset = $this->path->baseUrl . 'app/assets';
        $this->path->view = $this->path->basePath . 'app/views';
        $this->path->resource = $this->path->basePath . 'resources';
        $this->path->public = $this->path->basePath . 'public';
    }

    /**
     * @return void
     */
    protected function loadConfig()
    {
        $config = require $this->path->basePath . 'app/config.php';

        foreach ($config as $key => $value) {
            $this->config->$key = $value;
        }
    }

    /**
     * @return void
     * @throws \ReflectionException
     */
    protected function registerRoutes()
    {
        $restNamespace = $this->config->rest['namespace'];
        $restVersion = $this->config->rest['version'];
        $router = new Router($this, $restNamespace, $restVersion);
        require_once $this->path->basePath . 'app/routes/api.php';
        $this->addAction('rest_api_init', [$router, 'dispatch']);
        $this->container->singleton(Router::class, $router);
        $this->container->setAlias('router', Router::class);
    }

    /**
     * @return void
     */
    protected function loadHooks()
    {
        $app = $this;
        require $this->path->basePath . 'app/Hooks/action.php';

    }


    /**
     * @return void
     */
    public function boot()
    {
        $this->addAction('init', function () {
            $this->loadTextDomain('hasanmisbah', false, $this->path->basePath . 'languages');
        });
    }


    /**
     * @return void
     */
    public function beforeBoot()
    {
        App::setInstance($this);
    }

    /**
     * @return void
     */
    public function afterBoot()
    {
        // TODO: Implement afterBoot() method.
    }


    /**
     * @throws \Exception
     */
    public function handleRestRequest($handler, $parameters = [])
    {

        try {

            $response = $this->call($handler, $parameters);

            if (is_wp_error($response)) {

                return new \WP_REST_Response([
                    'message' => $response->get_error_message(),
                ], $response->get_error_code());

            }

            return new \WP_REST_Response($response, 200);

        } catch (\Exception $e) {

            return new \WP_REST_Response([
                'message' => $e->getMessage(),
            ], $e->getCode());
        }

    }

    /**
     * @param $handler
     * @return void
     * @throws \ReflectionException
     */
    public function handleModuleBoot($handler)
    {
        $this->call($handler);
    }

    /**
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        if ($this->isHook($name)) {
            return $this->callHook($name, $arguments);
        }

        return $this->forwardCallTo($name, $arguments, $this->container);
    }

    /**
     * @return View
     */
    public function view()
    {
        return $this->view;
    }

    /**
     * @return AccessContainer
     */
    public function path()
    {
        return $this->path;
    }

    /**
     * @return AccessContainer
     */
    public function config()
    {
        return $this->config;
    }

    /**
     * @throws \ReflectionException
     */
    public function registerServiceProvider()
    {
        $this->bindExtendedClasses();
        $this->bootModule();
    }

    /**
     * @return void
     * @throws \ReflectionException
     */
    private function bindExtendedClasses()
    {
        $classes = $this->call([ServiceProvider::class, 'bind']);

        foreach ($classes as $class) {

            if (is_array($class)) {
                $this->container->bind($class[0], $class[1]);
            } else {
                $this->container->bind($class);
            }
        }
    }

    /**
     * @return void
     * @throws \ReflectionException
     */
    private function bootModule()
    {
        $modules = $this->call([ServiceProvider::class, 'boot']);

        foreach ($modules as $module) {
            $this->handleModuleBoot($module);
        }
    }

    /**
     * @param $name
     * @return mixed|object|null
     * @throws \Exception
     */
    public function __get($name)
    {
        return $this->container->get($name);
    }

    /**
     * @return \HasanMisbah\Core\Support\Cache
     * @throws \Exception
     */
    public function cache()
    {
        return $this->get(Cache::class);
    }


    /**
     * @return \HasanMisbah\Core\Support\Http
     * @throws \Exception
     */
    public function http()
    {
        return $this->get(Http::class);
    }

    /**
     * @return void
     */
    public function registerActivationHook()
    {
        register_activation_hook($this->path->file, function () {
            $this->make(ActivationHandler::class)->run();
        });
    }

    /**
     * @return void
     */
    public function registerDeactivationHook()
    {
        register_deactivation_hook($this->path->file, function () {
            $this->make(DeactivationHandler::class)->run();
        });
    }

    /**
     * @return \HasanMisbah\Core\Routing\Router
     * @throws \Exception
     */
    public function router()
    {
        return $this->get(Router::class);
    }

    /**
     * @param $callable
     * @param $parameters
     * @return mixed|object|null
     * @throws \ReflectionException
     */
    public function call($callable, $parameters = [])
    {
        return $this->container->call($callable, $parameters);
    }

}
