<?php

namespace HasanMisbah\Core\Foundation;

/**
 * @method addAction($hook_name, callable $callback, $priority = 10, $accepted_args = 1)
 * @method addFilter($hook_name, callable $callback, $priority = 10, $accepted_args = 1)
 * @method addShortcode($tag, callable $callback)
 * @method doAction($tag, $arg = '')
 * @method doActionRefArray($tag, $args)
 * @method applyFilters($tag, $value)
 * @method addMenuPage(string $page_title, string $menu_title, string $capability, string $menu_slug, callable $callback = '', string $icon_url = '', int|float $position = null)
 * @method registerRestRoute(string $namespace, string $route, array $args = [], bool $override = false)
 * @method enqueueScript($handle, $src = '', $deps = array(), $ver = false, $args = array())
 * @method localizeScript($handle, $object_name, $l10n)
 * @method registerScript($handle, $src, $deps = array(), $ver = false, $args = array())
 * @method enqueueStyle($handle, $src = '', $deps = array(), $ver = false, $media = 'all')
 * @method updateOption($option, $value, $autoload = null)
 * @method getOption($option, $default = false)
 * @method addOption($option, $value = '', $deprecated = '', $autoload = 'yes')
 * @method loadTextDomain($domain, $deprecated = false, $plugin_rel_path = false)
 *
 * */
trait HookBinder
{
    /**
     * @var string[]
     */
    protected $hooks = [
        'addAction'         => 'add_action',
        'addFilter'         => 'add_filter',
        'addShortcode'      => 'add_shortcode',
        'doAction'          => 'do_action',
        'doActionRefArray'  => 'do_action_ref_array',
        'applyFilters'      => 'apply_filters',
        'removeAction'      => 'remove_action',
        'addMenuPage'       => 'add_menu_page',
        'registerRestRoute' => 'register_rest_route',
        'enqueueScript'     => 'wp_enqueue_script',
        'localizeScript'    => 'wp_localize_script',
        'registerScript'    => 'wp_register_script',
        'enqueueStyle'      => 'wp_enqueue_style',
        'updateOption'      => 'update_option',
        'getOption'         => 'get_option',
        'addOption'         => 'add_option',
        'loadTextDomain'    => 'load_plugin_textdomain',
    ];

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed|void
     */
    protected function callHook($name, $arguments)
    {
        return call_user_func_array($this->hooks[$name], $arguments);
    }

    protected function isHook($name)
    {
        return isset($this->hooks[$name]);
    }
}
