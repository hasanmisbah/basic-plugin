<?php

namespace HasanMisbah\Core\Foundation;

use HasanMisbah\Core\Request\Request;
use HasanMisbah\Core\Routing\Router;
use HasanMisbah\Core\Support\Cache;

/**
 * @template-extends \HasanMisbah\Core\Foundation\Application
 * @method static Cache cache()
 * @method static Application app()
 *
 * */
class App
{
    /**
     * @var \HasanMisbah\Core\Foundation\Application $app
     */
    static $instance;

    /**
     * @param \HasanMisbah\Core\Foundation\Application $instance
     * @return void
     */
    public static function setInstance($instance)
    {
        static::$instance = $instance;
    }

    /**
     * @param string|null $module
     * @returns Cache
     * @returns Application
     * @returns Request
     * @returns Router
     *
     */
    public static function getInstance($module = null)
    {
        if (is_null($module)) {
            return static::$instance;
        }

        return static::$instance->{$module};
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return static::$instance->{$name}(...$arguments);
    }
}
