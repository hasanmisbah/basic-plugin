<?php

namespace HasanMisbah\Core\Foundation;

trait ForwardCall
{
    /**
     * @param $method
     * @param $arguments
     * @param $object
     * @return mixed
     * @throws \Exception
     */
    public function forwardCallTo($method, $arguments, $object = null)
    {

        if (is_null($object)) {
            $object = $this;
        }

        if(method_exists($object, $method)) {
            return call_user_func_array([$object, $method], $arguments);
        }

        throw new \Exception("Method {$method} does not exist");

    }
}
