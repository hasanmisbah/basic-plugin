<?php

namespace HasanMisbah\Core\Request;
/**
 * Class Request
 * the request class is responsible for handling all the request data
 * the idea of the request class is from laravel
 * */

class Request
{
    /**
     * @var string|null
     */
    public $method = null;

    /**
     * @var false|string|null
     */
    public $body = null;

    /**
     * @var array
     */
    public $params = [];

    /**
     * @var array|false
     */
    public $headers = [];

    /**
     * @var array
     */
    public $cookies = [];

    /**
     * @var array
     */
    public $files = [];

    /**
     * @var array
     */
    public $server = [];

    /**
     * @var array
     */
    public $query = [];

    /**
     * @var array
     */
    public $post = [];

    /**
     * @var array
     */
    public $get = [];

    /**
     * @var array
     */
    public $request = [];

    /**
     * @var array|false|int|string|null
     */
    public $url = [];

    /**
     * @var array|false|string[]
     */
    public $uri = [];

    /**
     * @var array|false|string[]
     */
    public $urlParts = [];

    /**
     * @var array|false|string[]
     */
    public $urlPath = [];

    /**
     * @var array|false|string[]
     */
    public $urlQuery = [];

    /**
     * @var array|false|string[]
     */
    public $urlFragment = [];

    /**
     * @var array|false|string[]
     */
    public $urlHost = [];

    /**
     * @var array|false|string[]
     */
    public $urlPort = [];


    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->body = file_get_contents('php://input');
        $this->params = $_REQUEST;
        $this->headers = getallheaders();
        $this->cookies = $_COOKIE;
        $this->files = $_FILES;
        $this->server = $_SERVER;
        $this->query = $_GET;

        $this->post = $_POST;
        $this->request = $_REQUEST;
        $this->url = parse_url($_SERVER['REQUEST_URI']);
        $this->uri = explode('/', isset($this->url['path']) ? $this->url['path'] : '');
        $this->urlParts = explode('/', isset($this->url['path']) ? $this->url['path'] : '');
        $this->urlPath = explode('/', isset($this->url['path']) ? $this->url['path'] : '');
        $this->urlQuery = explode('/', isset($this->url['query']) ? $this->url['query'] : '');
        $this->urlFragment = explode('/', isset($this->url['fragment']) ? $this->url['fragment'] : '');
        $this->urlHost = explode('/', isset($this->url['host']) ? $this->url['host'] : '');
        $this->urlPort = explode('/', isset($this->url['port']) ? $this->url['port'] : '');
    }

    /**
     * @return array
     */
    public function all()
    {
        return array_merge($this->params, $this->query);
    }

    /**
     * @param $key
     * @param $default
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        $params = $this->all();
        return $this->has($key) ? $params[$key] : $default;
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        $params = $this->all();
        return isset($params[$key]);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function file($key)
    {
        if (isset($this->files[$key])) {
            return $this->files[$key];
        }

        return null;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function server($key)
    {
        if (!isset($this->server[$key])) {
            return null;
        }


        return $this->server[$key];
    }

    /**
     * @return false|string|null
     */
    public function body()
    {
        return $this->body;
    }

    /**
     * @return mixed|null
     */
    public function method()
    {
        return $this->method;
    }

    /**
     * @return array
     */
    public function params()
    {
        return $this->params;
    }

    /**
     * @return array
     */
    public function query()
    {
        return $this->query;
    }

    /**
     * @return array
     */
    public function files()
    {
        return $this->files;
    }

    /**
     * @return array|false
     */
    public function headers()
    {
        return $this->headers;
    }

    /**
     * @return array
     */
    public function cookies()
    {
        return $this->cookies;
    }

    public function getSanitized($key, callable $sanitizer = null, $default = null)
    {
        // check if the key is an associative array
        if (is_array($key)) {

            $sanitized = [];

            foreach ($key as $k => $s) {
                $sanitized[$k] = $this->getSanitized($k,  $s);
            }

            return $sanitized;
        }

        $value = $this->get($key, $default);
        $sanitizedValue = $this->sanitize($value, $sanitizer);

        return $sanitizedValue ?: $default;
    }

    public function sanitize($value, callable $sanitizer)
    {
        if (is_array($value)) {

            foreach ($value as $key => $val) {
                $value[$key] = $this->sanitize($val, $sanitizer);
            }
        } else {
            $value = $sanitizer($value);
        }

        return $value;
    }

    public static function capture()
    {
        return new static();
    }

    public function __get($name)
    {
        return $this->get($name);
    }
}
