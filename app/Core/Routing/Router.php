<?php

namespace HasanMisbah\Core\Routing;


class Router
{
    /**
     * @var null
     */
    protected $app = null;

    /**
     * @var string
     */
    protected $nameSpace = 'hasanmisbah';

    /**
     * @var string
     */
    protected $restVersion = 'v1';

    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @var array
     */
    protected $permissions = [];

    /**
     * @param $app
     * @param $nameSpace
     * @param $restVersion
     */
    public function __construct($app, $nameSpace, $restVersion)
    {
        $this->app = $app;
        $this->nameSpace = $nameSpace;
        $this->restVersion = $restVersion;
    }

    /**
     * @param $permissions
     * @return $this
     */
    public function permissions($permissions)
    {
        if(is_string($permissions)) {
            $permissions = [$permissions];
        }

        $this->permissions = $permissions;
        return $this;
    }

    public function group($options, \Closure $callback)
    {
        $this->options = $options;
        $callback($this);
        $this->options = [];
    }

    /**
     * @param $uri
     * @param $handler
     * @return Route
     */
    public function get($uri, $handler)
    {
        $this->routes[] = $route = $this->newRoute( $uri, $handler, 'GET');
        return $route;
    }

    /**
     * @param $uri
     * @param $handler
     * @return Route
     */
    public function post($uri, $handler)
    {
        $this->routes[] = $route = $this->newRoute($uri, $handler, 'POST');
        return $route;
    }

    /**
     * @param $uri
     * @param $handler
     * @return Route
     */
    public function put($uri, $handler)
    {
        $this->routes[] = $route = $this->newRoute($uri, $handler, 'PUT');
        return $route;
    }

    /**
     * @param $uri
     * @param $handler
     * @return Route
     */
    public function patch($uri, $handler)
    {
        $this->routes[] = $route = $this->newRoute($uri, $handler, 'PATCH');
        return $route;
    }

    /**
     * @param $uri
     * @param $handler
     * @return Route
     */
    public function delete($uri, $handler)
    {
        $this->routes[] = $route = $this->newRoute($uri, $handler, \WP_REST_Server::DELETABLE);
        return $route;
    }

    /**
     * @param $uri
     * @param $handler
     * @return Route
     */
    public function any($uri, $handler)
    {
        $this->routes[] = $route = $this->newRoute($uri, $handler, \WP_REST_Server::ALLMETHODS);
        return $route;
    }

    /**
     * @param $uri
     * @param $handler
     * @param $method
     * @return Route
     */
    protected function newRoute($uri, $handler, $method)
    {
        $route = Route::create(
            $this->app,
            $this->getRestNamespace(),
            $uri,
            $handler,
            $method
        );

        if ($this->permissions) {
            $route->permissions($this->permissions);
        }

        return $route;
    }

    /**
     * @return string
     */
    protected function getRestNamespace()
    {
        return ltrim($this->nameSpace . '/' . $this->restVersion);
    }

    /**
     * @return void
     */
    public function dispatch()
    {
        foreach ($this->routes as $route) {
            $route->register();
        }
    }
}
