<?php

namespace HasanMisbah\Core\Routing;

use HasanMisbah\Core\Exceptions\DuplicateRouteParameterException;

class Route
{
    /**
     * @var \HasanMisbah\Core\Foundation\Application $app
     * */
    protected $app = null;

    /**
     * @var null
     */
    protected $restNamespace = null;

    /**
     * @var null
     */
    protected $uri = null;

    /**
     * @var null
     */
    protected $compiled = null;

    /**
     * @var array
     */
    protected $meta = [];

    /**
     * @var null
     */
    protected $handler = null;

    /**
     * @var null
     */
    protected $method = null;

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var array
     */
    protected $wheres = [];

    /**
     * @var array
     */
    protected $permissions = [];

    /**
     * @var string[]
     */
    protected $predefinedNamedRegx = [
        'int'            => '[0-9]+',
        'alpha'          => '[a-zA-Z]+',
        'alpha_num'      => '[a-zA-Z0-9]+',
        'alpha_num_dash' => '[a-zA-Z0-9-_]+'
    ];


    /**
     * @param $app
     * @param $restNamespace
     * @param $uri
     * @param $handler
     * @param $method
     */
    public function __construct($app, $restNamespace, $uri, $handler, $method)
    {
        $this->app = $app;
        $this->restNamespace = $restNamespace;
        $this->uri = $uri;
        $this->handler = $handler;
        $this->method = $method;
    }

    /**
     * @param $app
     * @param $namespace
     * @param $uri
     * @param $handler
     * @param $method
     * @return static
     */
    public static function create($app, $namespace, $uri, $handler, $method)
    {
        return new static($app, $namespace, $uri, $handler, $method);
    }


    /**
     * @param $permissions
     * @return $this
     */
    public function permissions($permissions)
    {
        if (is_string($permissions)) {
            $permissions = [$permissions];
        }

        $this->permissions = array_merge($this->permissions, $permissions);

        return $this;
    }

    /**
     * @private
     * @return void
     * @throws \Exception
     */
    public function register()
    {
        $this->setOptions();

        $uri = $this->compileRoute($this->uri);

        $this->app->registerRestRoute(
            $this->restNamespace,
            "/{$uri}",
            $this->options
        );
    }

    /**
     * @return void
     */
    protected function setOptions()
    {
        $this->options = [
            'args'                => [
                '__meta__' => $this->meta
            ],
            'methods'             => $this->method,
            'callback'            => [$this, 'callback'],
            'permission_callback' => [$this, 'permissionCallback']
        ];
    }

    /**
     * @param $uri
     * @return string
     * @throws DuplicateRouteParameterException
     */
    protected function compileRoute($uri)
    {
        $params = [];

        $compiledUri = preg_replace_callback('#/{(.*?)}#', function ($match) use (&$params, $uri) {
            // Default regx
            $regx = '[^\s(?!/)]+';

            $param = trim($match[1]);

            if ($isOptional = strpos($param, '?')) {
                $param = trim($param, '?');
            }

            if (in_array($param, $params)) {
                throw new DuplicateRouteParameterException(
                    "Duplicate parameter name '{$param}' found in {$uri}.", 500
                );
            }

            $params[] = $param;

            if (isset($this->wheres[$param])) {
                $regx = $this->wheres[$param];
            }

            $pattern = '/(?P<' . $param . '>' . $regx . ')';

            if ($isOptional) {
                $pattern = '(?:' . $pattern . ')?';
            }

            $this->options['args'][$param]['required'] = !$isOptional;

            return $pattern;

        }, $uri);

        return $this->compiled = $compiledUri;
    }

    /**
     * @param \WP_REST_Request $request
     * @return mixed
     * @throws \Exception
     */
    public function callback(\WP_REST_Request $request)
    {
       return $this->app->handleRestRequest($this->handler, $request->get_url_params());
    }

    /**
     * @param \WP_REST_Request $request
     * @return bool
     */
    public function permissionCallback(\WP_REST_Request $request)
    {
        if (empty($this->permissions)) {
            return true;
        }

        $isPermitted = true;

        foreach ($this->permissions as $permission) {
            if(!current_user_can($permission)) {
                $isPermitted = false;
                break;
            }
        }

        return $isPermitted;
    }

    /**
     * @param $request
     * @return void
     */
    protected function setRestRequest($request)
    {

    }
}
