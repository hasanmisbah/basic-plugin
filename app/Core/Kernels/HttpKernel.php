<?php

namespace HasanMisbah\Core\Kernels;

use HasanMisbah\Core\Foundation\Application;

class HttpKernel implements HttpKernelInterface
{

    /**
     * @var Application $application
     * */
    protected $application = null;

    public function __construct(Application $application)
    {
        $this->application = $application;
    }


    public function bootstrap()
    {

    }

    public function register()
    {
        // TODO: Implement register() method.
    }


    public function boot()
    {

    }

    /**
     *  Handle the request
     */
    public function handle()
    {
        // :TODO register error handler and handle all response
        // :TODO ensure every request is properly handled and maintained
        // :TODO Request should captured in here and request handler should handled by the application
        // $this->boot();
    }
}
