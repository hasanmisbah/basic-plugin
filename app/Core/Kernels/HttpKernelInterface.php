<?php

namespace HasanMisbah\Core\Kernels;

use HasanMisbah\Core\Foundation\Application;

interface HttpKernelInterface
{
    public function handle();

    public function bootstrap();

    public function register();

    public function boot();
}
