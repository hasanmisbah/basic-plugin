<?php

namespace HasanMisbah\Core\Kernels;

use HasanMisbah\ServiceProvider;

class WPKernel extends HttpKernel
{
    public function registerActivationHook()
    {
        $this->application->registerActivationHook();
    }

    public function registerDeActivationHook()
    {
        $this->application->registerDeactivationHook();
    }

    /**
     * @throws \ReflectionException
     */
    public function registerServiceProvider()
    {
        $this->application->registerServiceProvider();
    }

    public function adminInit()
    {
        $this->application->call([ServiceProvider::class, 'adminInit']);
    }

    public function init()
    {
        $this->application->call([ServiceProvider::class, 'init']);
    }


    public function loadServiceProviderHook()
    {
        $this->application->addAction('init', [$this, 'init']);
        $this->application->addAction('admin_init', [$this, 'adminInit']);
    }

    /**
     * @throws \ReflectionException
     */
    public function handle()
    {
        $this->registerActivationHook();
        $this->registerDeActivationHook();
        $this->registerServiceProvider();
        $this->loadServiceProviderHook();
        $this->boot();
    }

    public function boot()
    {
        $this->application->beforeBoot();
        $this->application->boot();
        $this->application->afterBoot();
    }
}
