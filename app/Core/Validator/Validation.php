<?php

namespace HasanMisbah\Core\Validator;

class Validation
{
    protected $rules = [];

    protected $messages = [];

    protected $data = [];

    protected $customMessages = [];

    protected $errors = [];

    protected $errorBag = 'default';

    protected $errorFormat = ':message';

    protected $errorFormatter;

    protected $fallbackMessages = [];

    protected $replacers = [];

    protected $sizeRules = ['Size', 'Between', 'Min', 'Max'];

    protected $numericRules = ['Numeric', 'Integer'];

    protected $implicitRules = [
        'Required',
        'Filled',
        'RequiredWith',
        'RequiredWithAll',
        'RequiredWithout',
        'RequiredWithoutAll',
        'RequiredIf',
        'RequiredUnless',
        'Accepted',
        'Present',
    ];

    protected $dependentRules = [
        'RequiredWith',
        'RequiredWithAll',
        'RequiredWithout',
        'RequiredWithoutAll',
        'RequiredIf',
        'RequiredUnless',
    ];

    protected $excludeRules = [
        'ExcludeIf',
        'ExcludeUnless',
    ];

    protected $extensions = [];

    protected $fallbackMessagesLoaded = false;

    protected $container;

    public function __construct(array $data = [], array $rules = [], array $messages = [])
    {
        $this->init($data, $rules, $messages);
    }

    public function make(array $data = [], array $rules = [], array $messages = [])
    {
        return $this->init($data, $rules, $messages);
    }

    protected function init(array $data = [], array $rules = [], array $messages = [])
    {
        $this->data = $data;

        $this->setRules($rules);

        $this->messages = [];

        $this->customMessages = $messages;

        return $this;
    }

    protected function setRules(array $rules = [])
    {
        $this->rules = $this->parseRules($rules);

        return $this;
    }

    protected function parseRules(array $rules = [])
    {
        $parsedRules = [];

        foreach ($rules as $attribute => $rule) {
            if (strpos($attribute, '*') !== false) {
                $parsedRules = $this->parseWildcardRules($rules, $attribute, [$rule]);

                unset($parsedRules[$attribute]);
            } else {
                $parsedRules[$attribute] = $this->parseExplicitRule($rule);
            }
        }

        return $parsedRules;
    }

    protected function parseWildcardRules(array $rules, $attribute, $rule)
    {
        $keys = [];

        $pattern = str_replace('\*', '[^\.]+', preg_quote($attribute));

        foreach ($rules as $key => $value) {
            if ((bool) preg_match('/^'.$pattern.'/', $key, $matches)) {
                $keys[] = $matches[0];
            }
        }

        $keys = array_unique($keys);

        $parsedRules = [];

        foreach ($keys as $key) {
            $parsedRules[$key] = $this->parseWildcardAttribute($rules, $key, $rule);
        }

        return $parsedRules;
    }

    protected function parseWildcardAttribute($rules, $attribute, $rule)
    {
        $parsedRules = [];

        foreach ((array) $rule as $r) {
            $parsedRules[] = $this->parseRule($attribute, $r);
        }

        return $parsedRules;
    }

    protected function parseExplicitRule($rule)
    {
        if (is_string($rule)) {
            return $this->parseRule($rule, $rule);
        }

        return $rule;
    }

    protected function parseRule($attribute, $rule)
    {
        $parameters = [];

        if (strpos($rule, ':') !== false) {
            list($rule, $parameter) = explode(':', $rule, 2);

            $parameters = $this->parseParameters($rule, $parameter);
        }

        return compact('attribute', 'rule', 'parameters');
    }

    protected function parseParameters($rule, $parameter)
    {
        if ($this->isNamedParameter($rule, $parameter)) {
            return $this->getNamedParameters($rule, $parameter);
        }

        return explode(',', $parameter);
    }

    protected function isNamedParameter($rule, $parameter)
    {
        return ! in_array($rule, $this->numericRules) && ! in_array($rule, $this->sizeRules) && ! in_array($rule, $this->excludeRules) && ! in_array($rule, $this->dependentRules) && strpos($parameter, ',') === false;
    }

    protected function getNamedParameters($rule, $parameter)
    {
        return [$parameter];
    }

    public function validate()
    {
        foreach ($this->rules as $attribute => $rules) {
            foreach ($rules as $rule) {
                $this->validateAttribute($attribute, $rule);
            }
        }

        return $this;
    }

    protected function validateAttribute($attribute, $rule)
    {
        list($rule, $parameters) = $this->parseRule($attribute, $rule);

        $value = $this->getValue($attribute);

        if ($this->shouldValidate($rule, $attribute, $value)) {
            $this->validateAttributeWithRule($attribute, $rule, $parameters);
        }
    }

    protected function shouldValidate($rule, $attribute, $value)
    {
        return $this->validateRequired($attribute, $value) || $this->isImplicit($rule) || $this->hasRule($attribute, $this->dependentRules);
    }

    protected function validateRequired($attribute, $value)
    {
        if (! $this->hasRule($attribute, $this->implicitRules) && $this->isEmpty($value)) {
            return false;
        }

        return $this->validateRequiredIf($attribute) && $this->validateRequiredWith($attribute) && $this->validateRequiredWithout($attribute) && $this->validateRequiredWithoutAll($attribute) && $this->validateRequiredWithAll($attribute) && $this->validateRequiredIf($attribute) && $this->validateRequiredUnless($attribute);
    }

    protected function isEmpty($value)
    {
        return $value === null || $value === '' || is_array($value) && count($value) < 1;
    }

    protected function validateRequiredIf($attribute)
    {
        if ($this->hasRule($attribute, 'RequiredIf')) {
            list($other, $value) = $this->getRequiredIfValue($attribute);

            if ($this->validateRequired($other, $value)) {
                return $this->validateRequired($attribute, $this->getValue($attribute));
            }
        }

        return true;
    }

    protected function getRequiredIfValue($attribute)
    {
        $parameters = $this->getRule($attribute, 'RequiredIf');

        return [$parameters[0], $parameters[1]];
    }

    protected function validateRequiredWith($attribute)
    {
        if ($this->hasRule($attribute, 'RequiredWith')) {
            $values = $this->getRequiredWithValues($attribute);

            if ($this->allFulfilled($values)) {
                return $this->validateRequired($attribute, $this->getValue($attribute));
            }
        }

        return true;
    }

    protected function getRequiredWithValues($attribute)
    {
        return $this->getRule($attribute, 'RequiredWith');
    }

    protected function allFulfilled($values)
    {
        foreach ($values as $key => $value) {
            if (! $this->validateRequired($key, $this->getValue($key))) {
                return false;
            }
        }

        return true;
    }

    protected function validateRequiredWithout($attribute)
    {
        if ($this->hasRule($attribute, 'RequiredWithout')) {
            $values = $this->getRequiredWithoutValues($attribute);

            if (! $this->anyFulfilled($values)) {
                return $this->validateRequired($attribute, $this->getValue($attribute));
            }
        }

        return true;
    }

    protected function getRequiredWithoutValues($attribute)
    {
        return $this->getRule($attribute, 'RequiredWithout');
    }

    protected function anyFulfilled($values)
    {
        foreach ($values as $key => $value) {
            if ($this->validateRequired($key, $this->getValue($key))) {
                return true;
            }
        }

        return false;
    }

    protected function validateRequiredWithoutAll($attribute)
    {
        if ($this->hasRule($attribute, 'RequiredWithoutAll')) {
            $values = $this->getRequiredWithoutAllValues($attribute);

            if (! $this->allFulfilled($values)) {
                return $this->validateRequired($attribute, $this->getValue($attribute));
            }
        }

        return true;
    }

    protected function getRequiredWithoutAllValues($attribute)
    {
        return $this->getRule($attribute, 'RequiredWithoutAll');
    }

    protected function validateRequiredWithAll($attribute)
    {
        if ($this->hasRule($attribute, 'RequiredWithAll')) {
            $values = $this->getRequiredWithAllValues($attribute);

            if ($this->allFulfilled($values)) {
                return $this->validateRequired($attribute, $this->getValue($attribute));
            }
        }

        return true;
    }

    protected function getRequiredWithAllValues($attribute)
    {
        return $this->getRule($attribute, 'RequiredWithAll');
    }

    protected function validateRequiredUnless($attribute)
    {
        if ($this->hasRule($attribute, 'RequiredUnless')) {
            list($other, $value) = $this->getRequiredUnlessValues($attribute);

            if (! $this->validateRequired($other, $value)) {
                return $this->validateRequired($attribute, $this->getValue($attribute));
            }
        }

        return true;
    }

    protected function getRequiredUnlessValues($attribute)
    {
        $parameters = $this->getRule($attribute, 'RequiredUnless');

        return [$parameters[0], $parameters[1]];
    }

    protected function isImplicit($rule)
    {
        return in_array($rule, $this->implicitRules);
    }

    protected function hasRule($attribute, $rules)
    {
        return ! is_null($this->getRule($attribute, $rules));
    }

    protected function getRule($attribute, $rules)
    {
        foreach ($this->rules[$attribute] as $rule) {
            if (in_array($rule['rule'], (array) $rules)) {
                return $rule['parameters'];
            }
        }
    }

    protected function validateAttributeWithRule($attribute, $rule, $parameters)
    {
        $method = 'validate'.ucfirst($rule);

        if ($this->hasMethod($method)) {
            $this->$method($attribute, $parameters);
        }
    }

    protected function hasMethod($method)
    {
        return method_exists($this, $method);
    }

    protected function validateFilled($attribute, $value)
    {
        if ($this->hasRule($attribute, 'Filled')) {
            return ! $this->isEmpty($value);
        }

        return true;
    }

    public function getValue($attribute)
    {
        if (array_key_exists($attribute, $this->data)) {
            return $this->data[$attribute];
        }

        return null;
    }
}
