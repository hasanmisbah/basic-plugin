<?php

namespace HasanMisbah\Core\Validator;

class Validator
{
    protected $rules = [];

    protected $messages = [];

    protected $data = [];


    public function __construct(array $data = [], array $rules = [], array $messages = [])
    {
        $this->init($data, $rules, $messages);
    }

    public function make(array $data = [], array $rules = [], array $messages = [])
    {
        return $this->init($data, $rules, $messages);
    }

    protected function init(array $data = [], array $rules = [], array $messages = [])
    {
        $this->data = $data;
        $this->setRules($rules);
        $this->messages = $messages;
        return $this;
    }

    protected function setRules(array $rules = [])
    {
        $this->rules = $this->parseRules($rules);
        return $this;
    }

    protected function parseRules(array $rules = [])
    {
        $parsedRules = [];

        foreach ($rules as $attribute => $rule) {
            if (strpos($attribute, '*') !== false) {
                $parsedRules = $this->parseWildcardRules($rules, $attribute, [$rule]);
                unset($parsedRules[$attribute]);
            } else {
                $parsedRules[$attribute] = $this->parseExplicitRule($rule);
            }
        }
        return $parsedRules;
    }

    protected function parseWildcardRules(array $rules, $attribute, $rule)
    {
        $keys = [];
        $pattern = str_replace('\*', '[^\.]+', preg_quote($attribute));

        foreach ($rules as $key => $value) {
            if ((bool) preg_match('/^'.$pattern.'/', $key, $matches)) {
                $keys[] = $matches[0];
            }
        }

        $keys = array_unique($keys);

        foreach ($keys as $key) {
            $rules[$key] = $rule;
        }

        return $rules;
    }

    protected function parseExplicitRule($rule)
    {
        if (is_string($rule)) {
            return explode('|', $rule);
        }

        return $rule;
    }

    /**
     * @throws \Exception
     */
    public function validate()
    {
        foreach ($this->rules as $attribute => $rules) {
            foreach ($rules as $rule) {
                $this->validateAttribute($attribute, $rule);
            }
        }

        return $this;
    }

    /**
     * @throws \Exception
     */
    protected function validateAttribute($attribute, $rule)
    {
        list($rule, $parameters) = $this->parseRule($rule);

        $value = $this->getValue($attribute);

        if ($this->shouldValidate($rule, $attribute, $value)) {
            $this->validateAttributeWithRule($attribute, $rule, $parameters);
        }
    }

    protected function parseRule($rule)
    {
        if (is_string($rule)) {
            return $this->parseStringRule($rule);
        }

        return $rule;
    }

    protected function parseStringRule($rule)
    {
        list($rule, $parameters) = array_pad(explode(':', $rule, 2), 2, []);

        $parameters = $this->parseParameters($parameters);

        return [$rule, $parameters];
    }

    protected function parseParameters($parameters)
    {
        if (is_string($parameters)) {
            return explode(',', $parameters);
        }

        return $parameters;
    }

    protected function shouldValidate($rule, $attribute, $value)
    {
        return $this->validateRequired($rule, $attribute, $value) ||
            $this->validateFilled($rule, $attribute, $value);
    }

    protected function validateRequired($rule, $attribute, $value)
    {
        return $rule == 'required' || $this->hasRule($attribute, 'required');
    }

    protected function validateFilled($rule, $attribute, $value)
    {
        return $rule == 'filled' || $this->hasRule($attribute, 'filled');
    }

    protected function hasRule($attribute, $rule)
    {
        return isset($this->rules[$attribute]) && in_array($rule, $this->rules[$attribute]);
    }

    protected function validateAttributeWithRule($attribute, $rule, $parameters)
    {
        $rule = $this->normalizeRule($rule);

        $method = 'validate'.$rule;

        if (! method_exists($this, $method)) {
            throw new \Exception("Validation rule [$rule] does not exist.");
        }

        if (! $this->$method($attribute, $parameters)) {
            $this->addFailure($attribute, $rule, $parameters);
        }
    }

    protected function normalizeRule($rule)
    {
        return str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $rule)));
    }

    protected function getValue($attribute)
    {
        return isset($this->data[$attribute]) ? $this->data[$attribute] : null;
    }

    protected function addFailure($attribute, $rule, $parameters)
    {
        $this->messages[$attribute][] = $this->getMessage($attribute, $rule, $parameters);
    }

    protected function getMessage($attribute, $rule, $parameters)
    {
        $message = $this->getCustomMessage($attribute, $rule);

        if (is_null($message)) {
            $message = $this->getDefaultMessage($attribute, $rule);
        }

        return $this->doReplacements($message, $attribute, $rule, $parameters);
    }

    protected function getCustomMessage($attribute, $rule)
    {
        return isset($this->messages[$attribute.'.'.$rule]) ? $this->messages[$attribute.'.'.$rule] : null;
    }

    protected function getDefaultMessage($attribute, $rule)
    {
        $lowerRule = strtolower($rule);

        if (in_array($lowerRule, $this->sizeRules)) {
            return $this->getSizeMessage($attribute, $rule);
        }

        return $this->translator->get('validation.'.$lowerRule);
    }

    protected function getSizeMessage($attribute, $rule)
    {
        $lowerRule = strtolower($rule);

        $type = $this->getAttributeType($attribute);

        $key = "validation.{$lowerRule}.{$type}";

        return $this->translator->get($key);
    }

    protected function getAttributeType($attribute)
    {
        foreach ($this->sizeRules as $rule) {
            if (strpos($attribute, $rule) === 0) {
                return $rule;
            }
        }

        return 'string';
    }

    protected function doReplacements($message, $attribute, $rule, $parameters)
    {
        $message = str_replace(':attribute', $attribute, $message);

        if (method_exists($this, $replacer = 'replace'.ucwords($rule).'Rule')) {
            $message = $this->$replacer($message, $attribute, $rule, $parameters);
        }

        return $message;
    }

    protected function replaceRequiredRule($message, $attribute, $rule, $parameters)
    {
        return str_replace(':other', $parameters[0], $message);
    }

    protected function replaceRequiredIfRule($message, $attribute, $rule, $parameters)
    {
        return str_replace(':other', $parameters[0], $message);
    }

    protected function replaceRequiredWithRule($message, $attribute, $rule, $parameters)
    {
        return str_replace(':other', $parameters[0], $message);
    }

    protected function replaceRequiredWithoutRule($message, $attribute, $rule, $parameters)
    {
        return str_replace(':other', $parameters[0], $message);
    }

    protected function replaceRequiredUnlessRule($message, $attribute, $rule, $parameters)
    {
        return str_replace(':other', $parameters[0], $message);
    }
}
