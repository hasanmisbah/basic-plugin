<?php

namespace HasanMisbah\Core\Support;

interface HttpClientDriver
{
    public function get($url, $data = []);

    public function post($url, $data = []);

    public function put($url, $data = []);

    public function patch($url, $data = []);

    public function delete($url, $data = []);

}
