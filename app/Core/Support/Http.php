<?php

namespace HasanMisbah\Core\Support;

use HasanMisbah\Core\Foundation\App;
use HasanMisbah\Core\Foundation\Application;

/**
 * @implements HttpClientDriver $client // Concrete implementation found at app/Abstraction/WPHttpClient.php
 * @method static get($url, $data = []);
 * @method static post($url, $data = []);
 * @method static put($url, $data = []);
 * @method static patch($url, $data = []);
 * @method static delete($url, $data = []);
 * */
class Http
{
    /**
     * @var HttpClientDriver|null
     */
    public $client = null;

    /**
     * @var Application|null
     */
    public $app = null;

    /**
     * @param Application $app
     * @param HttpClientDriver $client
     */
    public function __construct(Application $app, HttpClientDriver $client)
    {
        $this->app = $app;
        $this->client = $client;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->client->{$name}(...$arguments);
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \Exception
     */
    public static function __callStatic($name, $arguments)
    {
        $instance = App::getInstance('http');

        if(!$instance) {
            throw new \Exception('Http client not Bound to Application');
        }

        return $instance->{$name}(...$arguments);
    }
}
