<?php

namespace HasanMisbah\Core\Support;

use HasanMisbah\Core\Foundation\App;
use HasanMisbah\Core\Foundation\Application;
use HasanMisbah\Core\Foundation\ForwardCall;

/**
 * @implements CacheDriver $driver // concrete implementation found at app/Core/Abstracts/Transient.php
 * @method static put(string $key, mixed $data = [], int $expiration = 0);
 * @method static get(string $key);
 * @method static delete(string $key);
 * @method static remember(string $key, callable $callback, int $expiration = HOUR_IN_SECONDS);
 * */

class Cache
{
    use ForwardCall;

    /**
     * @var Application $app
     * */
    protected $app = null;

    /**
     * @var CacheDriver $driver
     * */
    protected $driver = null;

    public function __construct(Application $app, CacheDriver $driver)
    {
        $this->app = $app;
        $this->driver = $driver;
    }


    /**
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        return $this->forwardCallTo( $name, $arguments,  $this->driver);
    }

    /**
     * @param string $name
     * @param mixed $arguments
     * @throws \Exception
     */
    public static function __callStatic($name, $arguments)
    {
        $instance = App::getInstance('cache');

        if(!$instance) {
            throw new \Exception('Cache driver not Bound to Application');
        }

        return $instance->{$name}(...$arguments);
    }
}
