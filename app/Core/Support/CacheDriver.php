<?php

namespace HasanMisbah\Core\Support;

interface CacheDriver
{
    public function put($key, $data = [], $expiration = 0);

    public function get($key);

    public function delete($key);

    public function remember($key, $callback, $expiration = HOUR_IN_SECONDS);
}
