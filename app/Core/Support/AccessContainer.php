<?php

namespace HasanMisbah\Core\Support;

class AccessContainer
{
    /**
     * @var array
     */
    protected $accessible = [];

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return void
     */
    public function set($key, $value)
    {
       $this->accessible[$key] = $value;
    }

    /**
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    public function get($key = null, $default = null)
    {
        if (is_null($key)) {
            return $this->accessible;
        }

        $data = isset($this->accessible[$key]) ? $this->accessible[$key] : $default;

        return $data;
    }

    public function __get($name)
    {
        return $this->get($name);
    }

    public function __set($name, $value)
    {
        $this->set($name, $value);
    }

    public function __isset($name)
    {
        return isset($this->accessible[$name]);
    }

    public function __unset($name)
    {
        unset($this->accessible[$name]);
    }
}
