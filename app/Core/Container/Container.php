<?php

namespace HasanMisbah\Core\Container;

class Container
{

    /**
     * All the registered bindings.
     * @var array
     */
    protected $bindings = [];

    /**
     * All the registered instances.
     * @var array
     */
    protected $instances = [];

    /**
     * All the registered aliases.
     * @var array
     */
    protected $aliases = [];


    /**
     * register a binding in to the container
     *
     * @param $abstract
     * @param $concrete
     * @return $this
     * @throws \ReflectionException
     */
    public function bind($abstract, $concrete = null)
    {
        if ($this->isBound($abstract)) {
            return $this;
        }

        if (is_null($concrete)) {
            $concrete = $abstract;
        }

        if(is_object($concrete)) {
            $this->instances[$abstract] = $concrete;
        }

        if($concrete instanceof \Closure) {
            $object = $this->closureFactory($concrete);
            $this->bindings[$abstract] = $object;
            $this->instances[$abstract] = $object;
            return $this;
        }

        $this->bindings[$abstract] = $concrete;
        return $this;

    }

    /**
     * make an instance of the given class
     *
     * @param $abstract
     * @return mixed|object|null
     * @throws \Exception
     */
    public function make($abstract)
    {
        if ($this->isBound($abstract)) {
            return $this->resolve($abstract);
        }

        if($abstract instanceof \Closure) {
            return $this->closureFactory($abstract);
        }


        if(is_object($abstract)) {
            return $abstract;
        }

        if (class_exists($abstract)) {
            return $this->abstractFactory($abstract);
        }

        throw new \Exception("Can not resolve class {$abstract}");
    }

    /**
     * @param $abstract
     * @return mixed|object|null
     * @throws \ReflectionException
     */
    private function resolve($abstract)
    {
        if(isset($this->aliases[$abstract])) {
            $abstract = $this->aliases[$abstract];
        }

        if (isset($this->instances[$abstract])) {
            return $this->instances[$abstract];
        }

        $concrete = $this->bindings[$abstract];

        if ($concrete instanceof \Closure) {
            return $this->closureFactory($concrete);
        }

        $this->instances[$abstract] = $this->abstractFactory($concrete);

        return $this->instances[$abstract];
    }

    /**
     * @param \Closure $closure
     * @param $parameterOverride
     * @return mixed
     * @throws \ReflectionException
     * @throws \Exception
     */
    private function closureFactory(\Closure $closure, $parameterOverride = [])
    {
        $reflector = new \ReflectionFunction($closure);
        $params = $reflector->getParameters();
        $dependencies = $this->getDependencies($params, $parameterOverride);
        return $reflector->invokeArgs($dependencies);
    }

    /**
     * @param $concrete
     * @return object|null
     * @throws \ReflectionException
     * @throws \Exception
     */
    private function abstractFactory($concrete)
    {
        $reflector = new \ReflectionClass($concrete);
        $constructor = $reflector->getConstructor();

        if (is_null($constructor)) {
            return $reflector->newInstance();
        }

        $params = $constructor->getParameters();
        $dependencies = $this->getDependencies($params);

        return $reflector->newInstanceArgs($dependencies);
    }

    /**
     * @param $abstract
     * @return bool
     */
    public function isBound($abstract)
    {
        return isset($this->bindings[$abstract]) || isset($this->instances[$abstract]) || isset($this->aliases[$abstract]);
    }

    /**
     * Method are fully not implemented yet, later will add full support
     * @param $abstract
     * @param $concrete
     * @return $this
     * @throws \ReflectionException
     */
    public function singleton($abstract, $concrete = null)
    {
        return $this->bind($abstract, $concrete, true);
    }

    /**
     * @param array $params
     * @param array $parameterOverride
     * @return array
     * @throws \Exception
     */
    private function getDependencies($params, $parameterOverride = [])
    {
        $dependencies = [];

        foreach ($params as $param) {

            $dependency = $param->getType();

            if (is_null($dependency)) {
                $dependencies[] = $this->resolveNonClass($param, $parameterOverride);
            } else {
                $dependencies[] = $this->make($dependency->getName());
            }
        }

        return $dependencies;

    }

    /**
     * @param $param
     * @param $parameterOverride
     * @return mixed
     * @throws \Exception
     */
    private function resolveNonClass($param, $parameterOverride = [])
    {
        if (isset($parameterOverride[$param->name])) {
            return $parameterOverride[$param->name];
        }

        if ($param->isDefaultValueAvailable()) {
            return $param->getDefaultValue();
        }

        throw new \Exception("Can not resolve class {$param->name}");
    }

    /**
     * @param callable|array|string $callback
     * @param array $parameterOverride
     * @return mixed
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function call($callback, $parameterOverride = [])
    {
        if ($callback instanceof \Closure) {
            return $this->closureFactory($callback, $parameterOverride);
        }

        if (is_array($callback)) {
            list($class, $method) = $callback;
            $instance = $this->make($class);
            return $this->methodFactory($instance, $method, $parameterOverride);
        }

        if(is_string($callback) && class_exists($callback)) {
            return $this->abstractFactory($callback);
        }


        if (is_string($callback) && strpos($callback, '@') !== false) {
            list($class, $method) = explode('@', $callback);
            $instance = $this->make($class);
            return $this->methodFactory($instance, $method, $parameterOverride);
        }

        throw new \Exception("Can not resolve callback {$callback}");
    }

    /**
     * @param object $instance
     * @param callable $method
     * @param array $parameterOverride
     * @return mixed
     * @throws \ReflectionException
     * @throws \Exception
     */
    private function methodFactory($instance, $method, $parameterOverride = [])
    {
        $reflector = new \ReflectionMethod($instance, $method);
        $params = $reflector->getParameters();
        $dependencies = $this->getDependencies($params, $parameterOverride);
        return $reflector->invokeArgs($instance, $dependencies);
    }

    /**
     * @param $abstract
     * @param $concrete
     * @return void
     * @throws \ReflectionException
     */
    public function rebind($abstract, $concrete)
    {
        if (isset($this->instances[$abstract])) {
            unset($this->instances[$abstract]);
        }

        $this->bind($abstract, $concrete);
    }

    /**
     * @param $abstract
     * @return bool
     */
    public function has($abstract)
    {
        return isset($this->bindings[$abstract]) || isset($this->instances[$abstract]) || isset($this->aliases[$abstract]);
    }

    /**
     * @param $abstract
     * @return mixed|object|null
     * @throws \Exception
     */
    public function get($abstract)
    {
        if (!$this->has($abstract)) {
            throw new \Exception("Can not resolve class {$abstract}");
        }

        return $this->make($abstract);
    }

    /**
     * @param $alias
     * @param $abstract
     * @return void
     */
    public function setAlias($alias, $abstract)
    {
        $this->aliases[$alias] = $abstract;
    }
}
