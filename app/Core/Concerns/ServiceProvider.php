<?php

namespace HasanMisbah\Core\Concerns;

use HasanMisbah\Core\Foundation\Application;

interface ServiceProvider
{
    public function bind();

    public function boot();

    public function init(Application $app);

    public function adminInit(Application $app);
}
