<?php

return [

    'name' => 'Hasan Misbah',
    'slug' => 'hasanmisbah',
    'version' => '1.0.0',

    'rest' => [
        'namespace' => 'hasanmisbah',
        'version'   => 'v1',
    ],

    'drivers' => [
        'cache' => \HasanMisbah\Abstraction\Transient::class,
        'http' => \HasanMisbah\Abstraction\WPHttpClient::class,
    ]
];
