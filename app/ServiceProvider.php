<?php

namespace HasanMisbah;

use HasanMisbah\Core\Foundation\Application;
use HasanMisbah\Modules\AdminMenu\AdminMenu;
use HasanMisbah\Services\ApiDataService;
use HasanMisbah\Services\SettingService;
use HasanMisbah\Core\Concerns\ServiceProvider as ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    /**
     * all methods in this class will be bound to the application
     * @return array
     * @example return [
     *     ExampleClass::class,
     *     ExampleInterFace::class => ExampleClass::class
     *     ExampleInterFace::class => function(){
     *        return new ExampleClass();
     *    }
     * ]
     */
    public function bind()
    {
        return [
            SettingService::class,
            ApiDataService::class
        ];
    }

    /**
     * all methods in this class will be booted when the application is booted
     * @return string[]
     * @example return [
     *    ExampleClass::class,
     *   [ExampleClass::class, 'index']
     * ]
     */
    public function boot()
    {
        return [
            AdminMenu::class
        ];
    }

    /**
     * all methods in this class will be initialized when run the @`init` hook
     *
     */
    public function init(Application $app)
    {
//        $app->call(function(){
//
//        });
    }

    /**
     * all methods in this class will be initialized when run the @`adminInit` hook
     * */
    public function adminInit(Application $app)
    {
//        $app->call(function(){
//
//        });
    }
}
