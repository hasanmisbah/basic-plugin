<?php

namespace HasanMisbah\Hooks;

use HasanMisbah\Core\Foundation\Application;

class ActivationHandler
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function run()
    {
        $settings = $this->app->getOption($this->app->config()->slug . '_settings', null);

        if (!empty($settings)) {
            return;
        }

        $adminEmail = get_option('admin_email');
        $showDateInFormat = 'human_readable';
        $showRowInTable = 5;

        $defaultSettings = [
            'emails'              => [$adminEmail],
            'show_date_in_format' => $showDateInFormat,
            'show_row_in_table'   => $showRowInTable,
        ];

        $this->app->addOption($this->app->config()->slug . '_settings', maybe_serialize($defaultSettings));
    }
}
