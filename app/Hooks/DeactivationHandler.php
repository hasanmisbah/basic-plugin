<?php

namespace HasanMisbah\Hooks;

use HasanMisbah\Core\Foundation\Application;

class DeactivationHandler
{
    protected $app;
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function run()
    {
        // $this->app->deleteOption($this->app->config()->slug . '_settings');
    }
}
