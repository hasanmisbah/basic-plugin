<div class="app-container" id="app"></div>
<noscript>
  <div class='error' style='font-size: 24px; text-align: center; padding: 20px;'>
    <p> <?php echo __('Please enable javascript to use this app.', 'hasanmisbah') ?> </p>
  </div>
</noscript>
