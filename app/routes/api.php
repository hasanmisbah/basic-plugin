<?php

/**
 * @var \HasanMisbah\Core\Routing\Router $router
 */


$permissions = 'manage_options';

$router->get('/graph', [\HasanMisbah\Http\Controllers\GraphController::class, 'index'])->permissions($permissions);
$router->get('/pages', [\HasanMisbah\Http\Controllers\PageController::class, 'index'])->permissions($permissions);
$router->get('/settings', [\HasanMisbah\Http\Controllers\SettingController::class, 'index'])->permissions($permissions);
$router->post('/settings', [\HasanMisbah\Http\Controllers\SettingController::class, 'update'])->permissions($permissions);
