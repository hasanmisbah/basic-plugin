<?php

namespace HasanMisbah\Exceptions;

class ValidationException extends \Exception
{
    public $data;

    public function __construct($message, $data)
    {
        parent::__construct($message, 423);
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}
