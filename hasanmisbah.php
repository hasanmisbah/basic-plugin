<?php

/**
 * Hasan Misbah
 *
 * @package           HasanMisbah
 * @author            Hasan Misbah
 * @copyright         2023 Hasan Misbah
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name: Hasan Misbah
 * Plugin URI: https://hasanmisbah.com
 * Description: Test Plugin
 * Version: 1.0.0
 * Requires PHP: 7.4
 * Author: Hasan Misbah
 * Author URI: https://hasanmisbah.com
 * Text Domain: hasanmisbah
 * Domain Path: /languages
 * License: GPL v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */


defined('ABSPATH') or die('Direct access not allowed here!');

require_once __DIR__ . '/vendor/autoload.php';

call_user_func(function ($bootstrap) {
    return $bootstrap(__FILE__);
}, require(__DIR__ . '/boot/app.php'));
