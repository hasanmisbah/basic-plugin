## Basic WordPress Plugin Architecture

The main goal of this plugin is fetch data from an external API and display it in the admin panel.
And the application should be accessible for admin users only.

**features:**

1. **Home page** - where show all the data from the API as tabular data by respecting the application settings.
2. **Graph page** - where show all the data from the API as graph by respecting the application settings.
3. **Settings page** - where admin can update the application settings.

## Installation

1. go to `wp-content/plugins/` directory
2. clone this repository
3. run `composer dump-autoload && composer install` to generate autoload files
4. run `npm install && npm run build` to generate assets
5. activate plugin from admin panel

## Usage

1. go to `Hasan Misbah` menu
2. Click settings and update settings as you want

## Development

1. run `npm run watch` to watch asset changes
2. run `npm run build` to build assets
3. run `composer dump-autoload` to generate autoload files

## Project Structure

```
├── app # contains all php files
    ├── Abstraction # contains all abstract classes which are implemented relevent concern and bound via service provider and have to register via 'config.php' file
    ├── Core # Contain core classes
        ├── Foundation # contains all foundation classes like: `Containner`, 'HookBinder', `Application Bootstrapper` etc
        ├── Http # contains all http related classes
        ├── Request # contains all request related classes
        ├── Response # contains all response related classes
        ├── Routing # contains all routing related classes
        ├── Support # Contain all utility Classes
        ├── Validator # Contain all validation related classes
    ├── Hooks # Contain all hooks entry point `action`, `filter` and `activation` and `deactivation` hooks. the `action.php` file will be loaded automatically when application bootstrapped
    ├── Http # Contain all http related classes
        ├── Controllers # Contain all controllers
    ├── Modules # Contain all individual modules like: `AdminMenu`, `Taxonomy`, `Shortcode` etc. and all modules must be registered via `ServiceProvider.php` file
    ├── routes # Contain all routes. the `api.php` file will be loaded automatically when application bootstrapped
    ├── Services # Contain all services. all serveices must be registered via `ServiceProvider.php` file or have to instantiate manually
    ├── Views # Contain all views
    ├── config.php # Contain all configurations
    ├── ServiceProvider.php # Contain all modules binding
├── boot # Contain application bootstrapper. basically it will boot the application
├── public # Contain all public and compiled assets like: `css`, `js`, `images` etc
├── resources # Contain all resources like: uncompiled `css`, `js`, `images` etc
    ├── scripts # Contain all scripts
    ├── stylesheet # Contain all stylesheets
├── vendor # Contain all composer dependencies
├── node_modules # Contain all npm dependencies
├── .editorconfig # editor config file
├── .eslintrc.js # eslint config file
├── .gitattributes # git attributes file
├── .gitignore # git ignore file
├── composer.json # composer json file
├── composer.lock # composer lock file
├── package.json # npm package json file
├── webpack.mix.js # webpack mix file
|── README.md # readme file
|── hasanmisbah.php # plugin main file
|── webpack.config.js # webpack config file
```

## Abstraction
Implementation of all abstract classes is bound via service provider and has to register via `config.php` file
there are two abstract classes implemented. in application `Transient` and `WPHttpClient` are implemented.
you can replace them with any other cache system and http client. just need to update the `config.php` file.

**WPHttp Client**
the `WPHttpClient` class didn't handle any error or exception which are responded via status code `200`

## General

The main goal of this structure is to make the code more readable, maintainable and scalable.
The structure is inspired by [Laravel](https://laravel.com/) framework.
The structure is not mandatory. You can change it as you want.
But you have to update the `composer.json` file and `composer dump-autoload` command to generate autoload files.
The perspective of this architecture is to load the whole application from a single entry point
and control every module from single place.
So, it will be easy to maintain and scale.
Some of the WordPress core features are resolved via this application architecture.
Like: `adminMenu`, `action`, `filter` etc. to achieve future dependency injection and resolving vi container.

## Built-in Support

1. Dependency Injection. like: `Symfony` and `Laravel`
2. Auto wiring. support like: `Symfony`
3. Service Container. like: `Laravel`
4. Rest API Routing System. like: `Laravel`
5. Transient Cache support from the container which is fully replaceable with any other cache system.
   like: `redis`, `memcached` etc. just need to update the `config.php` file. App::cache() will return the cache
   instance
6. WPHttp Client support from the container which is fully replaceable with any other http client.
   like: `guzzle`, `curl` etc. just need to update the `config.php` file. App::http() will return the http client
   instance
7. Application instance from anywhere via `App::instance()` method. if you pass arguments to the method then it will
   load the load instance of the alias. like: `App::instance('cache')` will return the cache instance

## Aliases

1. `App` => `App::instance()`
2. `Cache` => `App::instance('cache')`
3. `Http` => `App::instance('http')`
4. `Request` => `App::instance('request')`
5. `View` => `App::instance('view')`

## Supported Hooks

___Note:___ All hooks are supported via `App` class. Like: `App::addAction()`, `App::addFilter()` etc.


1. `add_action` => `App::addAction()`
2. `add_filter` => `App::addFilter()`
3. `add_shortcode` => `App::addShortcode()`
4. `do_action` => `App::doAction()`
5. `do_action_ref_array` => `App::doActionRefArray()`
6. `apply_filters` => `App::applyFilters()`
7. `remove_action` => `App::removeAction()`
8. `add_menu_page` => `App::addMenuPage()`
9. `register_rest_route` => `App::registerRestRoute()`
10. `wp_enqueue_script` => `App::enqueueScript()`
11. `wp_localize_script` => `App::localizeScript()`
12. `wp_register_script` => `App::registerScript()`
13. `wp_enqueue_style` => `App::enqueueStyle()`
14. `update_option` => `App::updateOption()`
15. `get_option` => `App::getOption()`
16. `add_option` => `App::addOption()`
17. `load_plugin_textdomain` => `App::loadTextDomain()`
