import { createApp } from 'vue';
import { createRouter, createWebHashHistory } from 'vue-router';
import { createPinia } from 'pinia';
const pinia = createPinia();
import { routes } from './routes';

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'


import Application from './Application.vue';


const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

const app = createApp(Application);
app.use(router);
app.use(pinia);
app.use(ElementPlus)
app.mount('#app');
