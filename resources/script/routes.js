import Index from './view/Index.vue';
import Settings from './view/Settings.vue';
import Graph from './view/Graph.vue';

export const routes = [
  {
    path: '/',
    name: 'home',
    component: Index,
  },
  {
    path: '/settings',
    name: 'settings',
    component: Settings,
  },
  {
    path: '/graph',
    name: 'graph',
    component: Graph,
  },
];
