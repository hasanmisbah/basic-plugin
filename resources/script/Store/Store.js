import { defineStore } from 'pinia'
import { state, actions } from './storeDefault'
import { request } from '../util/request';
export const useSettings = defineStore('settings', {

  state: () => ({
    ...state,
    data: {
      settings: {
        emails: [],
        show_date_in_format: '',
        show_row_in_table: 5,
      }
    }
  }),

  actions: {
    ...actions,

    async updateSettings(data, { successCb = () => ({}), errorCb = () => ({})} = {}) {
      return await this.handleApiCall(async ()=> {
        const response = await request().post('/settings', data);
        this.data.settings = response.data;
      }, { successCb, errorCb })
    },

    async getSettings() {
      return await this.handleApiCall(async ()=> {
        const response = await request().get('/settings');
        this.data.settings = response.data;
      })
    }

  }
})

export const useData = defineStore('data', {
  state: () => ({
    ...state,
    data: {
      table: [],
      graph: []
    },
  }),

  getters: {

    getTableData(){
      return this.data.table?.data?.rows || [];
    },

    getGraphData(){
      return this.data.graph;
    },

    hasData(){
      return this.data.table.length > 0;
    }
  },

  actions: {
    ...actions,

    async getTopPages(){
      await this.handleApiCall(async ()=> {
        const response = await request().get('/pages');
        this.data.table = response.data;
      });
    },

    async getGraph(){
      await this.handleApiCall(async ()=> {
        const response = await request().get('/graph');
        this.data.graph = response.data;
      });
    },
  }
});
