export const state = {
  loading: false,
  data: {},
  error: null,
};

export const actions = {

  async handleApiCall( callback, options = {}){

    const defaultCallback = {
      successCb: () => ({}),
      errorCb: () => ({}),
      ...options
    }

    try {

      this.error = null;
      this.loading = true;
      const response = await callback();
      defaultCallback.successCb(response);

    }catch (e) {
      this.handleError(e);
      defaultCallback.errorCb(e);
    }
    finally {
      this.loading = false;
    }
  },

  handleError(e){

    if(e.responseJSON) {

      this.error = e.responseJSON.message || 'Something went wrong';

    }else {
      this.error = e.message || 'Something went wrong';
    }
  }
}
