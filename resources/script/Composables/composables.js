import { ElMessage } from 'element-plus';
import { NOTIFICATION_CALLBACK, NOTIFICATION_OPTION, NOTIFICATION_TYPE } from '../util/types';

/**
 * useNotify composable function - which show notification using element plus message component
 * @return {object}
 * @property { NOTIFICATION_CALLBACK } success - send success notification
 * @property { NOTIFICATION_CALLBACK } error - send error notification
 * @property { NOTIFICATION_CALLBACK } info - send info notification
 * @property { NOTIFICATION_CALLBACK } warning - send warning notification
 * */

export function useNotify() {
  /**
   * @param { string } message
   * @param { NOTIFICATION_OPTION } options
   * @return { ElMessage }
   * */
  const success = (message, options = {}) => {
    return sendNotification('success', message, options);
  };

  /**
   * @param {string} message
   * @param {NOTIFICATION_OPTION} options
   * @return {ElMessage}
   * */
  const error = (message, options = {}) => {
    return sendNotification('error', message, options);
  }

  /**
   * @param {string} message
   * @param {NOTIFICATION_OPTION} options
   * @return {ElMessage}
   * */
  const info = (message, options = {}) => {
    return sendNotification('info', message, options);
  }

  /**
   * @param {string} message
   * @param {NOTIFICATION_OPTION} options
   * @return {ElMessage}
   * */

  const warning = (message, options = {}) => {
    return sendNotification('warning', message, options);
  }

  /**
   * @param {NOTIFICATION_TYPE} type
   * @param {string} message
   * @param {NOTIFICATION_OPTION} options
   * @return {ElMessage}
   * */
  const sendNotification = (type, message, options = {}) => {

    let notificationOptions = {
      customClass: 'app-error-message',
      ...options
    };

    ElMessage({
      ...notificationOptions,
      message: message || 'Something went wrong',
      type: type,
    });
  }

  return {
    success,
    error,
    info,
    warning
  }
}
