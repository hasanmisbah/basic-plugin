import dayjs from 'dayjs';

/**
 * @param {string} date
 * @param {string} type
 * @return { string } formatted date
 * */
export const formatDate = (date, type) => {

  //multiply by 1000 as JavaScript counts in milliseconds since epoch (which is 01/01/1970), not seconds
  const unixDateToTimeStamps = date * 1000;

  if (type === 'human_readable') {
    return formatAsHumanReadable(unixDateToTimeStamps);
  }

  // return timestamp
  return formatAsTimeStamps(unixDateToTimeStamps);
};

export const formatAsTimeStamps = (date) => {
  return dayjs(date).format('YYYY-MM-DD HH:mm:ss');
}

export const formatAsHumanReadable = (date) => {
  return dayjs(date).format('MMMM D, YYYY');
}
