function restRequest(method, url, data, config = {}) {

  let $url = `${window?.hasanmisbah?.rest?.url}${url}`;

  const headers = {
    'X-WP-Nonce': window?.hasanmisbah?.nonce,
    accept: 'application/json',
    ...config
  };

  if (['DELETE', 'PUT', 'PATCH'].includes(method)) {
    headers['X-HTTP-Method-Override'] = method;
    method = 'POST';
  }

  return new Promise((resolve, reject) => {
    return window.jQuery.ajax({
      method: method,
      url: $url,
      data:data,
      headers: headers,
      success: (response) => {
        return resolve(response);
      },
      error: (response) => {
        return reject(response);
      }
    })
  });
}

function get(url, data) {
  return restRequest('GET', url, data);
}

function post(url, data) {
  return restRequest('POST', url, data);
}

function put(url, data) {
  return restRequest('PUT', url, data);
}

function patch(url, data) {
  return restRequest('PATCH', url, data);
}

function del(url, data) {
  return restRequest('DELETE', url, data);
}

export function request() {
  return {
    get,
    post,
    put,
    delete: del
  }
}

jQuery(document).ajaxSuccess((event, xhr, settings) => {
  const nonce = xhr.getResponseHeader('X-WP-Nonce');
  if (nonce && window.hasanmisbah) {
    xhr.setRequestHeader('X-WP-Nonce', window.hasanmisbah.nonce);
  }
});
