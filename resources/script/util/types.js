/**
 * define a type for notification type
 * @typedef {'success'|'error'|'info'|'warning'} NOTIFICATION_TYPE
 * */
export let NOTIFICATION_TYPE;

/**
 * define a type for notification options
 * @typedef {object} NOTIFICATION_OPTION
 * @property {string} customClass
 * @property {string} duration
 * @property {string|Component} icon
 * @property {boolean} showClose
 * @property {boolean} center
 * @property {string} offset
 * @property {string} type
 * @property {string} message
 * @property {boolean} dangerouslyUseHTMLString
 * @property {Function} onClose
 * @property {string|HTMLElement} appendTo
 * @property {boolean} grouping
 * @property {number} repeatNum
 */
export let NOTIFICATION_OPTION;

/**
 * define a type for notification callback
 * @callback NOTIFICATION_CALLBACK
 * @param {string} message
 * @param {NOTIFICATION_OPTION} options
 * */
export let NOTIFICATION_CALLBACK;
